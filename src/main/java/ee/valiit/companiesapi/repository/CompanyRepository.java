package ee.valiit.companiesapi.repository;

import ee.valiit.companiesapi.model.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CompanyRepository {

    @Autowired
    private JdbcTemplate jdbcTemplae;

    public List<Company> fetchCompanies() {
        List<Company> companies = jdbcTemplae.query("SELECT * FROM company",
                (row, rowNum) ->{
                    return new Company(
                        row.getInt("id"),
                        row.getString("name"),
                        row.getString("logo")
                    );
                }
        );

        return companies;
    }

    public Company fetchCompany(int id) {
        List<Company> companies = jdbcTemplae.query("SELECT * FROM company WHERE id = ?",
                new Object[]{id},
                (row, rowNum) ->{
                    return new Company(
                        row.getInt("id"),
                        row.getString("name"),
                        row.getString("logo")
                    );
                }
        );

        if(companies.size()>0){
            return companies.get(0);
        } else {
            return null;
        }
    }

    public void deleteCompany(int id) {
        jdbcTemplae.update("DELETE FROM company WHERE id = ?", id);


    }

    public void addCompany(Company company) {
        jdbcTemplae.update("INSERT INTO company (name, logo) VALUES (?, ?)", company.getName(), company.getLogo());

    }

    public void updateCompany(Company company) {
        jdbcTemplae.update("UPDATE company SET name = ?, logo = ? WHERE id = ?",
        company.getName(),company.getLogo(), company.getId());
    }
}

