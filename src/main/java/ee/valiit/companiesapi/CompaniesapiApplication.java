package ee.valiit.companiesapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CompaniesapiApplication {

    public static void main(String[] args) {
        SpringApplication.run(CompaniesapiApplication.class, args);
    }

}