package ee.valiit.companiesapi.controller;

import ee.valiit.companiesapi.model.Company;
import ee.valiit.companiesapi.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController("/")
@CrossOrigin("*")
public class CompaniesController {

    @Autowired
    private CompanyRepository companyRepository;

    @GetMapping("/companies")
    public List<Company> getCompanies() {
        return companyRepository.fetchCompanies();
    }

    @GetMapping("/company")
    public Company getCompany(@RequestParam("id") int id) {
        return companyRepository.fetchCompany(id);

    }

    @DeleteMapping("/company")
    public void deleteCompany(@RequestParam("id") int id) {
        companyRepository.deleteCompany(id);
    }

    @PostMapping("/company")
    public void addCompany (@RequestBody Company c) {
        companyRepository.addCompany(c);
    }
    @PutMapping("/company")
    public void updateCompany(@RequestBody Company y) {
        companyRepository.updateCompany(y);

    }


    @GetMapping
    public String getHello(@RequestParam("name") String nameParam, @RequestParam("profession") String professionParam) {
        return "Tere " + nameParam + " Sa oled ametilt " + professionParam;

    }

    // teeme klassi olemasoleva klassi sisse:
    @PostMapping("/person")
    public void savePerson(@RequestBody Person x) {
        System.out.println("Me saime sellise sisendi: " + x.getName() + ", " + x.getProfession());

    }
    @GetMapping("/employees")
    public List<Person> getEmployees() {
        return Arrays.asList(
                new Person("Ulle", "õpetaja"),
                new Person("Teet", "müügijuht"),
                new Person("Malle", "tegevjuht"),
                new Person("Rita", "pressiesindaja")

        );
    }

    public static class Person {
        private String name;
        private String profession;

        public Person() {

        }

        public Person(String name, String profession) {
            this.name = name;
            this.profession = profession;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getProfession() {
            return profession;
        }

        public void setProfession(String profession) {
            this.profession = profession;
        }
    }

}
